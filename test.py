#!/usr/bin/python
#
# from: https://www.leinenbock.com/downloading-option-chains-from-yahoo-finance/
import sys
import time
  
t = time.time()
  
from xml.etree.ElementTree import ElementTree
import urllib, urllib2
import dateutil.relativedelta as relativedelta
import dateutil.rrule as rrule
import datetime as dt
from datetime import date
  
class YQL(object):
    url = 'http://query.yahooapis.com/v1/public/yql'
    env = 'store://datatables.org/alltableswithkeys'
    format = 'xml'
  
    @classmethod
    def query(cls, string):
        q = urllib.quote(string)
        url = cls.url + '&'.join(('?q=%s' % q, 'env=%s' % cls.env,
                                   'format=%s' % cls.format))


        resp = urllib2.urlopen(url)
        return ElementTree(file=resp).getroot().find('results')[:]



def getStockInfo(sym):
    xml_stockInfo = YQL.query('select * from yahoo.finance.quotes where ' + 'symbol="%s"' %sym)

    """
    example URL:
    http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol=%22csco%22&env=store://datatables.org/alltableswithkeys&format=xml

    exmple return:
    <query yahoo:count="1" yahoo:created="2014-01-16T03:39:20Z" yahoo:lang="en-US">
        <results>
            <quote symbol="CSCO">
                <Ask>22.86</Ask>
                <AverageDailyVolume>46840100</AverageDailyVolume>
                <Bid>22.39</Bid>
                <AskRealtime>22.86</AskRealtime>
                <BidRealtime>22.39</BidRealtime>
                <BookValue>11.007</BookValue>
                <Change_PercentChange>+0.37 - +1.65%</Change_PercentChange>
                <Change>+0.37</Change>
                <Commission/>
                <ChangeRealtime>+0.37</ChangeRealtime>
                <AfterHoursChangeRealtime>N/A - N/A</AfterHoursChangeRealtime>
                <DividendShare>0.68</DividendShare>
                <LastTradeDate>1/15/2014</LastTradeDate>
                <TradeDate/>
                <EarningsShare>1.839</EarningsShare>
                <ErrorIndicationreturnedforsymbolchangedinvalid/>
                <EPSEstimateCurrentYear>1.98</EPSEstimateCurrentYear>
                <EPSEstimateNextYear>2.08</EPSEstimateNextYear>
                <EPSEstimateNextQuarter>0.48</EPSEstimateNextQuarter>
                <DaysLow>22.42</DaysLow>
                <DaysHigh>23.00</DaysHigh>
                <YearLow>19.98</YearLow>
                <YearHigh>26.49</YearHigh>
                <HoldingsGainPercent>- - -</HoldingsGainPercent>
                <AnnualizedGain/>
                <HoldingsGain/>
                <HoldingsGainPercentRealtime>N/A - N/A</HoldingsGainPercentRealtime>
                <HoldingsGainRealtime/>
                <MoreInfo>cn</MoreInfo>
                <OrderBookRealtime/>
                <MarketCapitalization>121.8B</MarketCapitalization>
                <MarketCapRealtime/>
                <EBITDA>14.058B</EBITDA>
                <ChangeFromYearLow>+2.80</ChangeFromYearLow>
                <PercentChangeFromYearLow>+14.01%</PercentChangeFromYearLow>
                <LastTradeRealtimeWithTime>N/A - <b>22.78</b></LastTradeRealtimeWithTime>
                <ChangePercentRealtime>N/A - +1.65%</ChangePercentRealtime>
                <ChangeFromYearHigh>-3.71</ChangeFromYearHigh>
                <PercebtChangeFromYearHigh>-14.01%</PercebtChangeFromYearHigh>
                <LastTradeWithTime>Jan 15 - <b>22.78</b></LastTradeWithTime>
                <LastTradePriceOnly>22.78</LastTradePriceOnly>
                <HighLimit/>
                <LowLimit/>
                <DaysRange>22.42 - 23.00</DaysRange>
                <DaysRangeRealtime>N/A - N/A</DaysRangeRealtime>
                <FiftydayMovingAverage>21.5518</FiftydayMovingAverage>
                <TwoHundreddayMovingAverage>23.3845</TwoHundreddayMovingAverage>
                <ChangeFromTwoHundreddayMovingAverage>-0.6045</ChangeFromTwoHundreddayMovingAverage>
                <PercentChangeFromTwoHundreddayMovingAverage>-2.59%</PercentChangeFromTwoHundreddayMovingAverage>
                <ChangeFromFiftydayMovingAverage>+1.2282</ChangeFromFiftydayMovingAverage>
                <PercentChangeFromFiftydayMovingAverage>+5.70%</PercentChangeFromFiftydayMovingAverage>
                <Name>Cisco Systems, In</Name>
                <Notes/>
                <Open>22.51</Open>
                <PreviousClose>22.41</PreviousClose>
                <PricePaid/>
                <ChangeinPercent>+1.65%</ChangeinPercent>
                <PriceSales>2.45</PriceSales>
                <PriceBook>2.04</PriceBook>
                <ExDividendDate>Jan  2</ExDividendDate>
                <PERatio>12.19</PERatio>
                <DividendPayDate>Jan 22</DividendPayDate>
                <PERatioRealtime/>
                <PEGRatio>1.34</PEGRatio>
                <PriceEPSEstimateCurrentYear>11.32</PriceEPSEstimateCurrentYear>
                <PriceEPSEstimateNextYear>10.77</PriceEPSEstimateNextYear>
                <Symbol>CSCO</Symbol>
                <SharesOwned/><
                ShortRatio>1.30</ShortRatio>
                <LastTradeTime>4:00pm</LastTradeTime>
                <TickerTrend>&nbsp;======&nbsp;</TickerTrend>
                <OneyrTargetPrice>23.57</OneyrTargetPrice>
                <Volume>61803332</Volume>
                <HoldingsValue/>
                <HoldingsValueRealtime/>
                <YearRange>19.98 - 26.49</YearRange>
                <DaysValueChange>- - +1.65%</DaysValueChange>
                <DaysValueChangeRealtime>N/A - N/A</DaysValueChangeRealtime>
                <StockExchange>NasdaqNM</StockExchange>
                <DividendYield>3.03</DividendYield>
                <PercentChange>+1.65%</PercentChange>
            </quote>
        </results>
    </query>
    """

    return xml_stockInfo



def isSymbolValid(sym):
    xml_stockInfo = getStockInfo(sym)
    if xml_stockInfo[0].findtext('ErrorIndicationreturnedforsymbolchangedinvalid') != '':
        """
        sample value returned if symbol not found:

        'No such ticker symbol. <a href=/l>Try Symbol Lookup</a> (Look up: <a href=/l?s=12345>12345</a>)'

        """
        return False
    else:
        return True



def getStockPrice(sym):
    xml_stockPrice = YQL.query('select LastTradePriceOnly from yahoo.finance.quotes where symbol="%s"' %sym)
    return xml_stockPrice[0][0].text



def getRiskFreeRate():
    """
    Pulls the current 1Mo Daily Treasury Yield Curve Rate from:
    http://www.treasury.gov/resource-center/data-chart-center/interest-rates/Pages/TextView.aspx?data=yield
    """
    url = "http://www.treasury.gov/resource-center/data-chart-center/interest-rates/Datasets/yield.xml"
    resp = urllib2.urlopen(url)

    root = ElementTree(file=resp).getroot()

    RiskFreeRate = "0"
    ## parse through all BC_1MONTH elements.  The last one is the most recent risk free rate
    for element in root.iter('BC_1MONTH'):
        if element.text != "0" and element.text is not None:
            RiskFreeRate = element.text

    return RiskFreeRate


def getOptionChains(sym):
    price = getStockPrice(sym)

    xml_dates = YQL.query('select * from yahoo.finance.option_contracts where ' + 'symbol="%s"' %sym)[0]
    xml_contracts = YQL.query('select * from yahoo.finance.options where ' + 'symbol="%s"' %sym  )

    """
    example url for 'CSCO':
    http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.options%20where%20symbol%3D%22csco%22%20AND%20expiration%3D%222014-01%22&env=store://datatables.org/alltableswithkeys&format=xml
    
    sample result:

    <query yahoo:count="1" yahoo:created="2014-01-15T19:16:59Z" yahoo:lang="en-US">
        <results>
            <optionsChain expiration="2014-01-31" symbol="csco">
                <option symbol="CSCO140118C00008000" type="C">
                    <strikePrice>8</strikePrice>
                    <lastPrice>14.40</lastPrice>
                    <change>0</change>
                    <changeDir/>
                    <bid>14.7</bid>
                    <ask>15</ask>
                    <vol>8</vol>
                    <openInt>114</openInt>
                    </option>
                <option symbol="CSCO140118C00010000" type="C">
                    <strikePrice>10</strikePrice>
                    <lastPrice>12.90</lastPrice>
                    <change>0.45</change>
                    <changeDir>Up</changeDir>
                    <bid>12.35</bid>
                    <ask>12.85</ask>
                    <vol>10</vol>
                    <openInt>283</openInt>
                    </option>
    """

    dates = []
    for attr in xml_dates:
         #print attr.tag, attr.text
         dates.append(attr.text)
  
    optionChain = []
  
    puts = []; calls = []
    for expiration in dates:
            xml_contracts = YQL.query('select * from yahoo.finance.options where ' + \
                'symbol="%s" AND expiration="' %sym + expiration +'"'  )
  
            expiration = rrule.rrule(rrule.MONTHLY, byweekday=(relativedelta.FR(3)), \
                            dtstart=date(int(expiration.split("-")[0]), int(expiration.split("-")[1]), 1))[0]
            expiration = str(expiration).split(" ")[0]
  
            for option in xml_contracts[0]:
  
                if (option.attrib['type']=='P'):
                    # print sym + ': price=' + price + ', put: strike=' + option.findtext('strikePrice') + ', bid=' + \
                    #         option.findtext('bid') + ', ask=' + option.findtext('ask') +  ', vol=' + \
                    #         option.findtext('vol') +', date='+ expiration + ', last=' + option.findtext('lastPrice') + \
                    #         ', open int=' + option.findtext('openInt')

                   puts.append(['p',option.findtext('strikePrice'),expiration,option.findtext('lastPrice')])
 

                if (option.attrib['type']=='C'):
                    # print sym + ': price=' + price + ', call: strike=' + option.findtext('strikePrice') + ', bid=' + \
                    #         option.findtext('bid') + ', ask=' + option.findtext('ask') +  ', vol=' + \
                    #         option.findtext('vol') +', date='+ expiration + ', last=' + option.findtext('lastPrice') + \
                    #         ', open int=' + option.findtext('openInt')

                   calls.append(['c',option.findtext('strikePrice'),expiration,option.findtext('lastPrice')])
  
    return puts, calls
  
  
if __name__ == '__main__':
    stock = sys.argv[1]
    puts, calls = getOptionChains(stock)

    for put in puts:
        print put

    for call in calls:
        print call

