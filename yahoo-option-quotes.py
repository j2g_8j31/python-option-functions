#!/usr/bin/python
#
# from: https://www.leinenbock.com/downloading-option-chains-from-yahoo-finance/
import sys
import time
  
t = time.time()
  
from xml.etree.ElementTree import ElementTree
import urllib, urllib2
import dateutil.relativedelta as relativedelta
import dateutil.rrule as rrule
import datetime as dt
from datetime import date
  
class YQL(object):
    url = 'http://query.yahooapis.com/v1/public/yql'
    env = 'store://datatables.org/alltableswithkeys'
    format = 'xml'
  
    @classmethod
    def query(cls, string):
        q = urllib.quote(string)
        url = cls.url + '&'.join(('?q=%s' % q, 'env=%s' % cls.env,
                                   'format=%s' % cls.format))

        """
        example url for 'CSCO':
        http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.options%20where%20symbol%3D%22csco%22%20AND%20expiration%3D%222014-01%22&env=store://datatables.org/alltableswithkeys&format=xml
        
        sample result:
        <query yahoo:count="1" yahoo:created="2014-01-15T19:16:59Z" yahoo:lang="en-US"><results><optionsChain expiration="2014-01-31" symbol="csco"><option symbol="CSCO140118C00008000" type="C"><strikePrice>8</strikePrice><lastPrice>14.40</lastPrice><change>0</change><changeDir/><bid>14.7</bid><ask>15</ask><vol>8</vol><openInt>114</openInt></option><option symbol="CSCO140118C00010000" type="C"><strikePrice>10</strikePrice><lastPrice>12.90</lastPrice><change>0.45</change><changeDir>Up</changeDir><bid>12.35</bid><ask>12.85</ask><vol>10</vol><openInt>283</openInt></option>
        """

        resp = urllib2.urlopen(url)
        return ElementTree(file=resp).getroot().find('results')[:]
  
  
def getChain(sym):
    xml_stockInfo = YQL.query('select * from yahoo.finance.quotes where ' + 'symbol="%s"' %sym)
    price = xml_stockInfo[0].findtext('Ask')

    xml_dates = YQL.query('select * from yahoo.finance.option_contracts where ' + 'symbol="%s"' %sym)[0]
    xml_contracts = YQL.query('select * from yahoo.finance.options where ' + 'symbol="%s"' %sym  )

    dates = []
    for attr in xml_dates:
         #print attr.tag, attr.text
         dates.append(attr.text)
  
    optionChain = []
  
    puts = []; calls = []
    for expiration in dates:
            xml_contracts = YQL.query('select * from yahoo.finance.options where ' + 'symbol="%s" AND expiration="' %sym + expiration +'"'  )
  
            expiration = rrule.rrule(rrule.MONTHLY, byweekday=(relativedelta.FR(3)), \
                            dtstart=date(int(expiration.split("-")[0]), int(expiration.split("-")[1]), 1))[0]
            expiration = str(expiration).split(" ")[0]
  
            for option in xml_contracts[0]:
  
                if (option.attrib['type']=='P'):
                   print sym + ': price=' + price + ', put: strike=' + option.findtext('strikePrice') + ', bid=' + option.findtext('bid') + \
                            ', ask=' + option.findtext('ask') +  ', vol=' + option.findtext('vol') +', date='+ \
                            expiration + ', last=' + option.findtext('lastPrice') + ', open int=' + option.findtext('openInt')
                   puts.append(['p',option.findtext('strikePrice'),expiration,option.findtext('lastPrice')])
  
                if (option.attrib['type']=='C'):
                   print sym + ': price=' + price + ', call: strike=' + option.findtext('strikePrice') + ', bid=' + option.findtext('bid') + \
                            ', ask=' + option.findtext('ask') +  ', vol=' + option.findtext('vol') +', date='+ \
                            expiration + ', last=' + option.findtext('lastPrice') + ', open int=' + option.findtext('openInt')
                   calls.append(['c',option.findtext('strikePrice'),expiration,option.findtext('lastPrice')])
  
    return puts, calls
  
  
if __name__ == '__main__':
    stock = sys.argv[1]
    getChain(stock)

